/**
 * SPDX-FileCopyrightText: 2024 Nextcloud GmbH and Nextcloud contributors
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import type { Component } from 'vue'

import HelpPage from './components/pages/Help.vue'
import DeviceIntegrationPage from './components/pages/DeviceIntegration.vue'
import FunctionalitiesPage from './components/pages/FunctionalitiesPage.vue'
import IntroPage from './components/pages/Intro.vue'
import WhatsNewPage from './components/pages/WhatsNew.vue'

interface IPageButton {
	to: string
	label: string
}

export interface IPage {
	id: string
	component: Component
	buttons: IPageButton[]
}

export default [
	{
		id: 'intro',
		component: IntroPage,
		buttons: [{
			to: 'whats-new',
			label: 'Quoi de neuf',
		}, {
			to: 'functionnalities',
			label: 'Fonctionnalités',
		}],
	},

	{
		id: 'functionnalities',
		component: FunctionalitiesPage,
		buttons: [{
			to: 'devices',
			label: 'Framaspace sur tous vos appareils',
		}],
	},

	{
		id: 'devices',
		component: DeviceIntegrationPage,
		buttons: [{
			to: 'help',
			label: "Plus d'informations sur Framaspace",
		}],
	},

	{
		id: 'help',
		component: HelpPage,
		buttons: [{
			to: 'whats-new',
			label: 'Quoi de neuf',
		}],
	},

	{
		id: 'whats-new',
		component: WhatsNewPage,
		buttons: [{
			to: 'close',
			label: "C'est parti",
		}],
	},

] as IPage[]
